package de.dhbw.media2share.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import de.dhbw.media2share.model.MediaBelongObject;
import de.dhbw.media2share.model.User;
import de.dhbw.media2share.persistence.DataProvider;
import de.dhbw.media2share.persistence.Provider;

/**
 * @author Clemens Diebold
 *
 */
public class MyLibraryView {

	//@Autowired
	DataProvider dataProvider;
	
	//@Autowired
	User sessionUser;
	
	private List<ViewCategory> categories = new ArrayList<ViewCategory>();
	
	
	public MyLibraryView(){
		dataProvider = new DataProvider();
		sessionUser = new User("ralf.75@web.de", "ralfinator75", "ralf.75@web.de", "Ralf", "Hocker", new Date() ,"Beethovenweg 1", "Karlsruhe", "76287");
		
		categories.add(new ViewCategory(7, "success", "&le;"));
		categories.add(new ViewCategory(14, "warning", "&le;"));
		categories.add(new ViewCategory(21, "danger", "&ge;"));
	}
	
	private int dateDiffInDays(Date date){
		long diff = new Date().getTime() - date.getTime();
		
		return (int) (diff / 86400000);
	}
	
	private ViewCategory getCategory(int days){
		if(days <= categories.get(0).getDays()){
			return categories.get(0);
		}
		else if(days <= categories.get(1).getDays()){
			return categories.get(1);
		}
		else {
			return categories.get(2);
		}
	}
	
	
	public List<ListItem> getListOfRentedMedias(){
		
		List<ListItem> viewList = new ArrayList<ListItem>();
		ListItem listc1 = new ListItem(categories.get(0));
		ListItem listc2 = new ListItem(categories.get(1));
		ListItem listc3 = new ListItem(categories.get(2));
		viewList.add(listc3);
		viewList.add(listc2);
		viewList.add(listc1);
		
		int days;
		ViewCategory category;
		for(MediaBelongObject mbo : dataProvider.getAllRentMediasFromUser(sessionUser)){
			days = dateDiffInDays(mbo.getRentObject().getDate());
			category = getCategory(days);
			MyLibraryViewRentObject o = new MyLibraryViewRentObject(mbo.getMediaObject().getTitle(), mbo.getRentObject().getRenter().getFullName(), days, category);
			if(category == listc1.getCategory()){
			listc1.add(o);
			}
			else if(category == listc2.getCategory()){
			listc2.add(o);
			}
			else if(category == listc3.getCategory()){
			listc3.add(o);
			}
		}
		
		return viewList;
	}
	
	public class ListItem{
		private List list;
		ViewCategory c;
		
		public ListItem(ViewCategory c){
			list = new ArrayList<MyLibraryViewRentObject>();
			this.c = c;
		}
		
		public ViewCategory getCategory() {
			return c;
		}

		public void add(MyLibraryViewRentObject o){
		list.add(o);
		}
		
		public List getList(){
			return list;
		}
		
		public String getColor(){
			return c.getColor();
		}
		
		public int getDays(){
			return c.getDays();
		}
		
		public String getDescription(){
			return c.getDescription();
		}
	}
}
