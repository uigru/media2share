package de.dhbw.media2share.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.dhbw.media2share.model.BookObject;
import de.dhbw.media2share.model.FilmObject;
import de.dhbw.media2share.model.MusicObject;

public class ListItem {
		
		List<BookObject> listBook = new ArrayList<>();
		List<FilmObject> listFilm = new ArrayList<>();
		List<MusicObject> listMusic = new ArrayList<>();
		
		public ListItem() {
			// TODO Auto-generated constructor stub
		}
		
		public void addBookList(List<BookObject> b){
			listBook.addAll(b);
		}
		
		public void addFilmList(List<FilmObject> f){
			listFilm.addAll(f);
		}

		public void addMusicList(List<MusicObject> m){
			listMusic.addAll(m);
		}
		
		public void addBook(BookObject b){
			listBook.add(b);
		}
		
		public void addFilm(FilmObject f){
			listFilm.add(f);
		}

		public void addMusic(MusicObject m){
			listMusic.add(m);
		}
		
		public List<BookObject> getListBook(){
			return listBook;
		}
		
		public List<FilmObject> getListFilm(){		
			return listFilm;
		}
		
		public List<MusicObject> getListMusic(){
			return listMusic;
		}
		
		public void sort(){
			Collections.sort(listMusic);
			Collections.sort(listFilm);
			Collections.sort(listBook);
		}
		
		public int getSize(){
			return listMusic.size() + listBook.size() + listFilm.size();
		}
		
}
