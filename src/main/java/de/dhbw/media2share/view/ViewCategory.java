package de.dhbw.media2share.view;

public class ViewCategory {
	
	private int days;
	private String color;
	String description;
	
	public ViewCategory(int days, String color, String description){
		this.days = days;
		this.color = color;
		this.description = description;
	}
	
	public int getDays(){
		return days;
	}
	
	public String getColor(){
		return color;
	}

	public String getDescription() {
		return description + " " + days + "  ";
	}

}
