package de.dhbw.media2share.view;

public class MyLibraryViewRentObject {

	private String title;
	private String renter;
	private int days;
	private ViewCategory category;
	
	public MyLibraryViewRentObject(){
		
	}
	
	public MyLibraryViewRentObject(String title, String renter, int days,
			ViewCategory category) {
		super();
		this.title = title;
		this.renter = renter;
		this.days = days;
		this.category = category;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getRenter() {
		return renter;
	}



	public void setRenter(String renter) {
		this.renter = renter;
	}



	public int getDays() {
		return days;
	}



	public void setDays(int days) {
		this.days = days;
	}



	public String getColor() {
		return category.getColor();
	}

	
	
}
