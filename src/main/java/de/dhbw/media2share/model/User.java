package de.dhbw.media2share.model;

import java.util.Date;


/**
 * @author Clemens Diebold
 *
 */
public class User {
	private String username;
	private String password;
	private String mail;
	
	private String foreName;
	private String lastName;
	private Date birthdate;
	private String street;
	private String city;
	private String zipCode;
	
	public User(){
		
	}
	
	public User(String username, String password, String mail, String foreName,
			String lastName, Date birthdate, String street, String city,
			String zipCode) {
		super();
		this.username = username;
		this.password = password;
		this.mail = mail;
		this.foreName = foreName;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.street = street;
		this.city = city;
		this.zipCode = zipCode;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getForeName() {
		return foreName;
	}
	public void setForeName(String foreName) {
		this.foreName = foreName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getFullName(){
		return foreName + " " + lastName;
	}
	
	@Override
	public boolean equals(Object o){
		return this.getUsername().equals(((User)o).getUsername());
		
	}
}
