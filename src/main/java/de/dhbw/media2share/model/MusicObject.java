package de.dhbw.media2share.model;


/**
 * @author Clemens Diebold
 *
 */
public class MusicObject extends MediaObject{
	
	public MusicObject(){
		super();
	}

	public MusicObject(String picture, String title, String description, String artists, MediaType mediaType) {
		super(picture, title, description, artists, mediaType);
	}
	
	

}
