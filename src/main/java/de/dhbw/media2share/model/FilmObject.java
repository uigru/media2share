package de.dhbw.media2share.model;


/**
 * @author Clemens Diebold
 *
 */
public class FilmObject extends MediaObject{

	public FilmObject(){
		super();
	}
	
	public FilmObject(String picture, String title, String description, String artists, MediaType mediaType){
		super(picture, title, description, artists, mediaType);
	}
}
