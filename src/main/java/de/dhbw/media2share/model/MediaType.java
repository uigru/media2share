package de.dhbw.media2share.model;


/**
 * @author Clemens Diebold
 *
 */
public enum MediaType {
	KASETTE, CD, DVD, BLURAY, SCHALLPLATTE, FILE, BOOK, eBook
}
