/**
 * 
 */
package de.dhbw.media2share.model;

/**
 * @author Clemens Diebold
 *
 */
public class MediaBelongObject {

	private User owner;
	private MediaObject mediaObject;
	private MediaRentObject rentObject;
	
	public MediaBelongObject(User owner, MediaObject mediaObject){
		this.owner = owner;
		this.mediaObject = mediaObject;
	}

	public User getOwner(){
		return owner;
	}
	
	public MediaObject getMediaObject(){
		return mediaObject;
	}

	public MediaRentObject getRentObject() {
		return rentObject;
	}
	
	public void setRentObject(MediaRentObject rentObject) {
		this.rentObject = rentObject;
	}
}
