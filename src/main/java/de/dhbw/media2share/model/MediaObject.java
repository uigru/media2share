package de.dhbw.media2share.model;


/**
 * @author Clemens Diebold
 *
 */
public class MediaObject implements Comparable<MediaObject>{
	
	private String title;
	private String description;
	private String artists;
	private String picture;
	private MediaType mediaType;
	
	public MediaObject(){
		
	}
	
	public MediaObject(String picture, String title, String description, String artists, MediaType mediaType){
		this.picture = picture;
		this.title = title;
		this.description = description;
		this.artists = artists;
		this.mediaType = mediaType;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getArtists() {
		return artists;
	}

	public void setArtists(String artists) {
		this.artists = artists;
	}
	
	public String getPicture(){
		return picture;
	}
	
	public void setPicture(String picture){
		this.picture = picture;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}
	
	public String getMediaTypeString(){
		return mediaType.toString();
	}

	@Override
	public int compareTo(MediaObject o) {
		return this.getTitle().compareToIgnoreCase(o.getTitle());
	}

}
