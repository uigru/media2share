package de.dhbw.media2share.model;



/**
 * @author Clemens Diebold
 *
 */
public class BookObject extends MediaObject{
	
	private int pages;
	
	public BookObject() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BookObject(String picture, String title, String description, String artists, int pages, MediaType mediaType) {
		super(picture, title, description, artists, mediaType);
		this.pages = pages;
	}

}
