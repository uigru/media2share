/**
 * 
 */
package de.dhbw.media2share.model;

import java.util.Date;

/**
 * @author Clemens Diebold
 *
 */
public class MediaRentObject {
	
	private User renter;
	private Date date;
	
	public MediaRentObject(User renter, Date date){
		this.renter = renter;
		this.date = date;
	}


	public User getRenter() {
		return renter;
	}

	public void setRenter(User renter) {
		this.renter = renter;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	

}
