package de.dhbw.media2share.web;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.dhbw.media2share.model.BookObject;
import de.dhbw.media2share.model.FilmObject;
import de.dhbw.media2share.model.MediaObject;
import de.dhbw.media2share.model.MediaType;
import de.dhbw.media2share.model.MusicObject;
import de.dhbw.media2share.persistence.DataProvider;
import de.dhbw.media2share.view.ListItem;

@WebServlet(urlPatterns={"/Search"})
public class SearchServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	@EJB
	private DataProvider dp;
	
	public ListItem getListItem(boolean book, boolean music, boolean film, boolean all, boolean barea, double distance, String search){
		ListItem listItem = new ListItem();
		if(distance < 15.0 && barea){
				List<MediaObject> results = dp.getAllMediaObjects(search);
					for(MediaObject m : results){
							if(book || all){
								if(m instanceof BookObject){
									listItem.addBook((BookObject)m);
								}
							}
							if(film || all){
								if(m instanceof FilmObject){
									listItem.addFilm((FilmObject)m);
								}
							}
							if(music || all){
								if(m instanceof MusicObject){
									listItem.addMusic((MusicObject)m);
								}
							}
					}
					listItem.sort();
		}
		return listItem;
	}
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String searchString = request.getParameter("searchfield");
		String mediatype = request.getParameter("categories");
		String area = request.getParameter("area");
		String distance = request.getParameter("distance");
		
		boolean book = false, music = false, film = false;
		boolean all = true;
		if(mediatype != null){
			if(mediatype.equals("option2")){
				book = true;
				all = false;
			}
			else if(mediatype.equals("option3")){
				film = true;
				all = false;
			}
			else if(mediatype.equals("option4")){
				music = true;
				all = false;
			}
			else{
				all = true;
			}
		}
		
		boolean barea = true;
		if(area != null){
			if(area.equals("option2")){
				barea = false;
			}
		}
		
		double ddistance = 0.0;
		if(distance != null && !distance.isEmpty()){
			try{
				ddistance = Double.valueOf(distance);
			}
			catch(NumberFormatException ex){
				
			}
		}
		
		ListItem item = new ListItem();
		
		String search = "";
		if(searchString != null){
			search = searchString;
		}
		
		item = getListItem(book, music, film, all, barea, ddistance, search);
		
		request.setAttribute("listItem", item);
		request.getRequestDispatcher("/searchResult.jsp").forward(request, response);
		
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
