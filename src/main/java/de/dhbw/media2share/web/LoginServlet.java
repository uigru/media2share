package de.dhbw.media2share.web;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.dhbw.media2share.model.User;
import de.dhbw.media2share.persistence.DataProvider;

@WebServlet(urlPatterns={"/Login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	private DataProvider dp;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		if(username != null && password != null){
			User user = dp.getUser(username);
				if(user != null){
					if(user.getPassword().equals(password)){
						request.getRequestDispatcher("/Main.jsp").forward(request, response);
					}
					else{
						request.setAttribute("alert", "Ihre Passworteingabe ist falsch");
						request.getRequestDispatcher("/login.jsp").forward(request, response);
					}
				}
				else{
					request.setAttribute("alert", "Der Benutzername ist falsch");
					request.getRequestDispatcher("/login.jsp").forward(request, response);
				}
		}
		else{
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
