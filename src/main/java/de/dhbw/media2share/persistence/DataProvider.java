package de.dhbw.media2share.persistence;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import de.dhbw.media2share.model.BookObject;
import de.dhbw.media2share.model.FilmObject;
import de.dhbw.media2share.model.MediaBelongObject;
import de.dhbw.media2share.model.MediaObject;
import de.dhbw.media2share.model.MediaRentObject;
import de.dhbw.media2share.model.MediaType;
import de.dhbw.media2share.model.MusicObject;
import de.dhbw.media2share.model.User;


/**
 * @author Clemens Diebold
 *
 */

@LocalBean
@Stateless
public class DataProvider implements Provider{
	
	private List<MediaObject> medias;
	private Map<String, User> users;
	private List<MediaBelongObject> mediaRelations;
	
	public DataProvider(){
		
		//Alle Demo Medien Objekte werden hier erstellt und dann der Liste medias hinzugefügt.
		MediaObject[] allMediaObjects = new MediaObject[36];
		
		allMediaObjects[0] = new FilmObject("0", "The dark knight", "Der Film ist die psychologische Auseinandersetzung mit dem Bösen, auch in sich selbst. Nicht nur Gotham City, sondern auch Batman  verfolgt von den eigenen Dämonen - steht am Abgrund.", "Christian Bale, Heath Ledger, Michael Caine, Maggie Gyllenhaal", MediaType.BLURAY);
		allMediaObjects[1] = new FilmObject("1", "Django unchained", "Angesiedelt in den Südstaaten, zwei Jahre vor dem Bürgerkrieg, erzählt Django Unchained die Geschichte von Django (Oscar-Preisträger Jamie Foxx), einem Sklaven, dessen brutale Vergangenheit mit seinen Vorbesitzern dazu führt, dass er dem deutschstämmigen Kopfgeldjäger Dr. King Schultz (Oscar-Preisträger Christoph Waltz) Auge in Auge gegenübersteht.", "Jamie Foxx, Christoph Waltz, Leonardo DiCaprio, Kerry Washington, Samuel L. Jackson", MediaType.BLURAY);
		allMediaObjects[2] = new FilmObject("2", "Two and a half Men Staffel 1", "Junggeselle Charlie Harper lebt im Paradies: Er hat ein Strandhaus in Malibu, einen überbezahlten Job, und er geht ständig mit den schönsten Frauen aus. Doch dann lässt sich sein verklemmter Bruder Alan scheiden und zieht bei Charlie ein und seinen zehnjährigen Sohn Jake bringt er auch gleich mit. ", "Charlie Sheen, Jon Cryer, Angus T. Jones", MediaType.DVD);
		allMediaObjects[3] = new FilmObject("3", "Dschungelbuch", "Im Mittelpunkt steht der kleine Junge Mowgli, der im Dschungel ausgesetzt und von Wölfen grossgezogen wurde. Als eines Tages der böse Tiger Shir Khan, der die Menschen hasst, im Urwald auftaucht, entschliesst sich die Wolfsfamilie, Mowgli sicherheitshalber zu den Menschen zu bringen.", "", MediaType.KASETTE);
		allMediaObjects[4] = new FilmObject("4", "Kindsköpfe 2", "Dieses Mal sind es die Erwachsenen, die von ihren Kindern einige Lektionen über das Leben lernen, an einem Tag, der bekannt dafür ist, voller Überraschungen zu stecken dem letzten Schultag. ", "Adam Sandler, Kevin James, Chris Rock, David Spade", MediaType.DVD);
		allMediaObjects[5] = new FilmObject("5", "Titanic", "Der internationale Superstar Leonardo DiCaprio und die Oscar-nominierte Kate Winslet bezaubern auf der Leinwand als die jungen Liebenden Jack und Rose, die auf der Jungfernfahrt der unsinkbaren R.M.S. Titanic zueinanderfinden. Als das schicksalhafte Luxusschiff im eisigen Nordatlantik mit einem Eisberg kollidiert, wird ihre leidenschaftliche Liebe zu einem packenden Wettlauf gegen den Tod.","Leonardo DiCaprio, Kate Winslet, Bill Paxton, Billy Zane", MediaType.BLURAY);
		allMediaObjects[6] = new FilmObject("6", "KeinOhrHasen","Um nicht ins Gefängnis zu wandern, muss Ludo 300 Stunden Sozialarbeit ableisten. Und das ausgerechnet im Kinderhort von Anna, der er während der gemeinsamen Schulzeit ganz übel mitgespielt hat. Natürlich nimmt Anna genüsslich Rache an Ludo und lässt ihn seine Taten schwer büssen.","Til Schweiger, Nora Tschirner, Matthias Schweighöfer", MediaType.DVD);
		allMediaObjects[7] = new FilmObject("7", "Avatar","Auf dem sechs Millionen Lichtjahre entfernten Mond Pandora hat man massive Vorkommen eines besonders wertvollen Rohstoffes entdeckt. Doch Pandora ist bewohnt. Die in ihrer spirituellen Verbundenheit an uns vertraute Ureinwohner erinnernden Navi leben dort im Einklang mit der unberührten Natur. Ein gross angelegtes Projekt unter der wissenschaftlichen Führung von Dr. Grace Augustine (Sigourney Weaver) soll nun den Kontakt zu den Navis intensivieren um schliesslich eine Umsiedlung vorzubereiten.","Sam Worthington, Zoss Saldasa, Sigourney Weaver", MediaType.DVD);
		allMediaObjects[8] = new FilmObject("8", "Black Swan","Für die ehrgeizige Ballerina Nina (Oscar-Preisträgerin Natalie Portman) wird ein Traum wahr, als sie die Hauptrolle in Schwanensee ergattert. Diese Doppelrolle entwickelt sich für sie jedoch zum Albtraum: Während sie die Unschuld des weissen Schwans perfekt verkörpert, stt sie als verruchter schwarzer Schwan an ihre prüden Grenzen.","Natalie Portman, Vincent Cassel, Mila Kunis", MediaType.DVD);
		allMediaObjects[9] = new FilmObject("9", "Vom Winde verweht","Die schöne und egozentrische Scarlett O'Hara hat während des amerikanischen Bürgerkrieges nur ein Ziel vor Augen: Die Bewahrung ihres Südstaaten-Gutes Tara, auf dem sie glücklich aufwuchs und sich unsterblich in den gutmütigen Ashley Wilkes verliebte. Da er schon Bräutigam der naiven Melanie ist, heiratet sie den Langweiler Charles. Die Ehe ist nicht von Dauer - Charles fällt im Krieg. Schliesslich lernt Scarlett den Herzensbrecher Rhett Butler kennen, dessen Zuneigung sie schamlos ausnützt.","Vivien Leigh, Clark Gable, Olivia de Havilland", MediaType.DVD);
		allMediaObjects[10] = new MusicObject("10", "Bad","","Michael Jackson", MediaType.CD);
		allMediaObjects[11] = new MusicObject("11", "Spiceworld","","Spicegirls", MediaType.CD);
		allMediaObjects[12] = new MusicObject("12", "Spektakulär","","Michael Wendler", MediaType.CD);
		allMediaObjects[13] = new BookObject("13", "Ein ganzes halbes Jahr","Will Traynor weiss, dass es nie wieder so sein wird wie vor dem Unfall. Und er weiss, dass er dieses neue Leben nicht führen will. Er weiss nicht, dass er schon bald Lou begegnen wird. Eine Frau und ein Mann. Eine Liebesgeschichte, anders als alle anderen.","Jojo Moyes", 7,MediaType.BOOK);
		allMediaObjects[14] = new BookObject("14", "Tödliche Saat - Ostfrieslandkrimi","Der grausame Mord an einem Journalisten führt Hauptkommissar David Bttner und seinen Assistenten Sebastian Hasenkrug ins idyllische ostfriesische Fischerdorf Greetsiel. Nach ersten Ermittlungen deutet vieles darauf hin, dass der junge Familienvater Opfer einer skrupellosen Agrarlobby wurde. Denn bereits seit längerem waren ihr seine engagierten Recherchen zum massiven Bienensterben und dem Einsatz genmanipulierten Getreides ein Dorn im Auge. Doch auch im Privatleben des Opfers lief nicht alles so harmonisch, wie es auf den ersten Blick schien.","Elke Bergsma", 7,MediaType.eBook);
		allMediaObjects[15] = new BookObject("15", "BLACKOUT - Morgen ist es zu spät","An einem kalten Februartag brechen in Europa alle Stromnetze zusammen. Der totale Blackout. Der italienische Informatiker Piero Manzano vermutet einen Hackerangriff und versucht, die Behörden zu warnen war erfolglos. Als Europol-Kommissar Bollard ihm endlich zuhört, tauchen in Manzanos Computer dubiose Emails auf, die den Verdacht auf ihn selbst lenken. Er ist ins Visier eines Gegners geraten, der ebenso raffiniert wie gnadenlos ist. Unterdessen liegt ganz Europa im Dunkeln, und der Kampf ums überleben beginnt Marc Elsberg","", 7,MediaType.BOOK);
		allMediaObjects[16] = new MusicObject("16", "No Line on the Horizon","","U2", MediaType.CD);
		allMediaObjects[17] = new MusicObject("17", "The Truth About Love","","Pink", MediaType.CD);
		allMediaObjects[18] = new MusicObject("18", "Bravo Hits Vol.84","","Various Artists", MediaType.CD);
		allMediaObjects[19] = new BookObject("19", "Die Analphabetin, die rechnen konnte","Die aberwitzige Geschichte der jungen Afrikanerin Nombeko, die zwar nicht lesen kann, aber ein Rechengenie ist, fast zufällig bei der Konstruktion nuklearer Sprengköpfe mithilft und nebenbei Verhandlungen mit den Mächtigen der Welt führt.","Jonas Jonasson", 7,MediaType.BOOK);
		allMediaObjects[20] = new BookObject("20", "Tschick","Maik Klingenberg wird die grossen Ferien allein am Pool der elterlichen Villa verbringen. Doch dann kreuzt Tschick auf. Tschick, eigentlich Andrej Tschichatschow, kommt aus einem der Asi-Hochhäuser in Hellersdorf, hat es von der Förderschule irgendwie bis aufs Gymnasium geschafft und wirkt doch nicht gerade wie das Musterbeispiel der Integration. Ausserdem hat er einen geklauten Wagen zur Hand. Und damit beginnt eine Reise ohne Karte und Kompass durch die sommerglühende deutsche Provinz, unvergesslich wie die Flussfahrt von Tom Sawyer und Huck Finn.","Wolfgang Herrndorf", 7,MediaType.BOOK);
		allMediaObjects[21] = new BookObject("21", "Er ist wieder da","Sommer 2011. Adolf Hitler erwacht auf einem leeren Grundstück in Berlin-Mitte. Ohne Krieg, ohne Partei, ohne Eva. Im tiefsten Frieden, unter Tausenden von Ausländern und Angela Merkel. 66 Jahre nach seinem vermeintlichen Ende strandet der Grfaz in der Gegenwart und startet gegen jegliche Wahrscheinlichkeit eine neue Karriere - im Fernsehen.","Timur Vermes", 7, MediaType.BOOK);
		allMediaObjects[22] = new MusicObject("22", "Lieder","","Adel Tawil", MediaType.CD);
		allMediaObjects[23] = new MusicObject("23", "Taking chances","","Celine Dion", MediaType.CD);
		allMediaObjects[24] = new MusicObject("24", "Farbenspiel","","Helene Fischer", MediaType.CD);
		allMediaObjects[25] = new BookObject("25", "Die Bibel: Altes und Neues Testament. Einheitsübersetzung","EZ ca. 10. Jahrhundert v. Chr. 2. Jahrhundert n. Chr.","Bischöfe Deutschlands und Österreichs und der Bistümer Bozen-Brixen und Lüttich", 7,MediaType.BOOK);
		allMediaObjects[26] = new BookObject("26", "Der Knochenbrecher","Im L.A. wird eine weibliche Leiche gefunden, die auf den ersten Blick unverletzt scheint. Unverletzt, bis auf ein winziges Detail: 2 Körperöffnungen sind zugenöht. Bei der Autopsie passiert dann das Unfassbare: eine in den Körper eingeführte Bombe explodiert. Für Robert Hunter und Carlos Garcia beginnt die Jagd nach einem Killer, der nicht so ist wie die anderen...","Chris Carter", 7, MediaType.BOOK);
		allMediaObjects[27] = new BookObject("27", "Der Junge im gestreiften Pyjama","Bruno wächst zu Beginn des zweiten Weltkrieges wohlbehütet und glücklich mit seiner Familie in Berlin auf. Doch dann muss er plötzlich an einen Ort namens Aus-Wisch umziehen, weil der Furor den Vater dort für eine wichtige Aufgabe vorgesehen hat.","John Boyne", 7,MediaType.eBook);
		allMediaObjects[28] = new MusicObject("28", "MGMT","","MGMT", MediaType.CD);
		allMediaObjects[29] = new MusicObject("29", "To Beast Or Not to Beast","","Lordi", MediaType.CD);
		allMediaObjects[30] = new MusicObject("30", "The Essential","2000","Elvis Presley",MediaType.CD);
        allMediaObjects[31] = new MusicObject("31", "G I R L","2014","Pharell Williams",MediaType.CD);
        allMediaObjects[32] = new BookObject("32", "50 Shades of Grey","When literature student Anastasia Steele interviews successful entrepreneur Christian Grey, she finds him very attractive and deeply intimidating. Convinced that their meeting went badly, she tries to put him out of her mind - until he turns up at the store where she works part-time, and invites her out.","E. L. James",534, MediaType.BOOK);
        allMediaObjects[33] = new BookObject("33", "Pleasures of the Damned","","Charles Bukowski",214, MediaType.BOOK);
        allMediaObjects[34] = new FilmObject("34", "300","Rise of an Empire - 2014","Noam Murro",MediaType.BLURAY);
        allMediaObjects[35] = new FilmObject("35", "Captain America","The Winter Soldier - 2014","Anthony Russo, Joe Russo",MediaType.BLURAY);
		
		
		medias = new ArrayList<MediaObject>();
		for(MediaObject m : allMediaObjects){
			medias.add(m);
		}
		
		
		// Alle User werden hier erstellt und dann der Liste Users hinzugef��gt.
		User[] user = new User[10];
		user[0] = new User("ralf.75@web.de", "ralfinator75", "ralf.75@web.de", "Ralf", "Hocker", new Date() ,"Beethovenweg 1", "Karlsruhe", "76287");
		user[1] = new User("laura@socialbusiness.de", "happygirl123", "laura@socialbusiness.de", "Laura", "Siebenstein", new Date(), "Akademieweg 32", "Karlsruhe", "76287");
		user[2] = new User();
		user[3] = new User();
		user[4] = new User();
		user[5] = new User();
		user[6] = new User();
		user[7] = new User();
		user[8] = new User();
		user[9] = new User();
		
		users = new HashMap<String, User>();
		for(User u : user){
			users.put(u.getUsername(), u);
		}

		
		//Alle MediaBelongObjekte für die Zuordnung welcher Benutzer welches MediaObject in seiner Mediathek hat
		MediaBelongObject[] mediaBelongObjects = new MediaBelongObject[50];
		mediaBelongObjects[0] = new MediaBelongObject(user[0], allMediaObjects[0]);
		mediaBelongObjects[1] = new MediaBelongObject(user[0], allMediaObjects[1]);
		mediaBelongObjects[2] = new MediaBelongObject(user[0], allMediaObjects[2]);
		mediaBelongObjects[3] = new MediaBelongObject(user[0], allMediaObjects[3]);
		mediaBelongObjects[4] = new MediaBelongObject(user[0], allMediaObjects[4]);
		mediaBelongObjects[5] = new MediaBelongObject(user[1], allMediaObjects[2]);
		mediaBelongObjects[5] = new MediaBelongObject(user[1], allMediaObjects[5]);
		mediaBelongObjects[5] = new MediaBelongObject(user[1], allMediaObjects[6]);
		
		mediaRelations = new ArrayList<MediaBelongObject>();
		for(MediaBelongObject mbo : mediaBelongObjects){
			mediaRelations.add(mbo);
		}

		
		// Alle MediaRentObjekte für die Zuordnung welcher Benutzer welches MedienObjekt wann ausgeliehen hat.	
		MediaRentObject[] mro = new MediaRentObject[30];
		mro[0] = new MediaRentObject(user[1], randomDate());
		mediaBelongObjects[1].setRentObject(mro[0]);
		mro[1] = new MediaRentObject(user[1], randomDate());
		mediaBelongObjects[2].setRentObject(mro[1]);
		mro[2] = new MediaRentObject(user[1], randomDate());
		mediaBelongObjects[3].setRentObject(mro[2]);
		mro[3] = new MediaRentObject(user[1], randomDate());
		mro[4] = new MediaRentObject(user[1], randomDate());
		mro[5] = new MediaRentObject(user[1], randomDate());
		mro[6] = new MediaRentObject(user[1], randomDate());
		mro[7] = new MediaRentObject(user[1], randomDate());
		mro[8] = new MediaRentObject(user[1], randomDate());
		mro[9] = new MediaRentObject(user[1], randomDate());
		mro[10] = new MediaRentObject(user[1], randomDate());
		mro[11] = new MediaRentObject(user[1], randomDate());
		
	}
	
	
	public static Date randomDate()
	{
		// create Calendar instance with actual date
		Date now = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(now);

		// substrate X days to calendar instance
		int x = (int) (Math.random() * -40);
		calendar.add(Calendar.DAY_OF_MONTH, x);

		// get the date instance
		Date past = calendar.getTime();
		
		return past;
	}

	public User getUser(String username){
		return users.get(username);
	}
	
	
	public List<MediaObject> getAllMediaObjects(String search){
		List<MediaObject> result = new ArrayList<MediaObject>();
		for(MediaObject m : medias){
			if(m.getTitle().contains(search) || m.getDescription().contains(search) ||m.getTitle().toLowerCase().contains(search.toLowerCase())  || m.getTitle().toUpperCase().contains(search.toUpperCase())){
				result.add(m);
				
			}
		}
		return result;
	}
	
	public List<String> getAllMediaObjectsTitle(){
		List<String> list = new ArrayList<String>();
		for(MediaObject m : medias){
			list.add(m.getTitle());
		}
		return list;
	}
	
	
	public List<MediaBelongObject> getAllRentMediasFromUser(User u) {
		
		List<MediaBelongObject> list = new ArrayList<MediaBelongObject>();
		
		for(MediaBelongObject m : mediaRelations){
			if(m != null && m.getOwner().equals(u) && m.getRentObject() != null){
				list.add(m);
			}
		}

		return list;
	}


	public void addUser(User user) {
		users.put(user.getUsername(), user);
	}


	public void removeUser(User user) {
		users.remove(user.getUsername());
	}


	public boolean addMediaObject(MediaObject mediaObject) {
		return medias.add(mediaObject);
	}


	public boolean removeMediaObject(MediaObject mediaObject) {
		return medias.remove(mediaObject);
	}


	public boolean addMediaObjectToUser(MediaObject mediaObject, User owner) {
		MediaBelongObject m = new MediaBelongObject(owner, mediaObject);
		return mediaRelations.add(m);
	}


	public boolean removeMediaObjectFromUser(MediaObject mediaObject, User owner) {
		MediaBelongObject m = new MediaBelongObject(owner, mediaObject);
		return mediaRelations.remove(m);
	}


	public boolean rentMedia(User renter, MediaBelongObject mediaBelongObject) {
		MediaRentObject mro = new MediaRentObject(renter, new Date());
		mediaBelongObject.setRentObject(mro);
		return true;
	}


	public boolean bringMediaBack(User renter, MediaBelongObject mediaBelongObject) {
		if(mediaBelongObject.getRentObject().getRenter().equals(renter)){
			mediaBelongObject.setRentObject(null);
			return true;
		}
		else{
			return false;
		}
	}
	
}
