package de.dhbw.media2share.persistence;

import java.util.List;

import de.dhbw.media2share.model.MediaBelongObject;
import de.dhbw.media2share.model.MediaObject;
import de.dhbw.media2share.model.User;

/**
 * @author Clemens Diebold
 * 
 */
public interface Queries {

	public boolean addUser(User user);

	public boolean removeUser(User user);

	public boolean addMediaObject(MediaObject mediaObject);

	public boolean removeMediaObject(MediaObject mediaObject);

	public boolean addMediaObjectToUser(MediaObject mediaObject, User user);

	public boolean removeMediaObjectFromUser(MediaObject mediaObject, User user);
	
	public boolean rentMedia(User renter, MediaBelongObject mediaBelongObject);
	
	public boolean bringMediaBack(User renter, MediaBelongObject mediaBelongObject);

	public List<MediaBelongObject> getAllRentMediasFromUser(User u);

}
