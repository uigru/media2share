<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landing Page after Login">
    <meta name="author" content="Clemens Diebold">
    
    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom Media2Sare CSS -->
	<link href="resources/css/media2share.css" rel="stylesheet">
	<!-- CSS config for the polarchart -->
	<link href="resources/css/chart.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
body{
overflow-x: Hidden;
overflow-y: Hidden;
}
</style>
    
<title>Xame</title>
</head>
<body onload="createPolarChart();">
	<jsp:include page="components/header.jsp"></jsp:include>
	<div class="container-fluid">
      <div class="row">
      		<jsp:include page="components/sidebarLeft.jsp"></jsp:include>
  			<div>

          			<div>
          				<jsp:include page="components/chart.jsp"></jsp:include>
          			</div>
          			
          			
            </div>
  			<jsp:include page="components/sidebarRight.jsp"></jsp:include>
  	 </div>
  	 </div>
  	 <jsp:include page="components/footer.jsp"></jsp:include>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/extendChart.js"></script>
</script>
</body>
</html>
