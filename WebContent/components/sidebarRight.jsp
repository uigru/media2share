<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<div class="sidebarRight">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><span class="glyphicon glyphicon-user"></span> Freunde</h3>
		</div>
		<div class="panel-body">[Endpoint] Freundesliste: Liste mit Freunden</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title"><span class="glyphicon glyphicon-globe"></span> Umkreissuche</h3>
		</div>
		<div class="panel-body">[Endpoint] Umkreissuche: Liste mit Medien in der Nähe, nach Entfernung sortiert</div>
	</div>
</div>
</html>