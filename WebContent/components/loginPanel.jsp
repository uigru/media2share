<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="de">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    				<script>
					function verschwinde() {
							$("#pwhd").hide();
							$("#vname").hide();
							$("#nname").hide();
							$("#geb").hide();
						};
						function erscheine() {
							$("#pwhd").show();
							$("#vname").show();
							$("#nname").show();
							$("#geb").show();
						};
				</script>
<div id="openModal" class="modalDialog">
	<div class="jumbotron">
		<a href="#close" title="Close" class="close">X</a>
		<h2>Registrieren</h2>
		<form class="form-horizontal" id="registerHere" method='GET' action='Register'>
			<fieldset>
				<legend>Xame ist ein freies und kostenloses Netzwerk.</legend>
				
				<div data-toggle="buttons">
					<label class="btn btn-default active" onclick="erscheine()"> <input type="radio" name="resultLayout-options" id="Xame" >Xame</label>
					<label class="btn btn-default" onclick="verschwinde()"> <input type="radio" name="resultLayout-options" id="facebook"><img src="resources/img/facebook.jpg" width="20" height="20"></label> 
					<label class="btn btn-default" onclick="verschwinde()"> <input type="radio" name="resultLayout-options" id="g+"><img src="resources/img/g+.jpg" width="20" height="20"></label>
					<label class="btn btn-default" onclick="verschwinde()"> <input type="radio" name="resultLayout-options" id="twitter"><img src="resources/img/twitter.jpg" width="20" height="20"></label>
			    </div>
				
				<br>
				
				<input type="email" id="user_name" name="user_name" value="${param.user_name}" class="form-control" placeholder="E-Mail Adresse" required="required">
				<input type="password" id="passwort" name="password" class="form-control" placeholder="Passwort" required="required">
				<span id="pwhd"><input type="password" id="passwort_wdh" class="form-control" placeholder="Passwort wiederholen"></span>
				<br> 
				<span id="vname"><input type="text" id="vorname" class="form-control" value="${param.vorname}" placeholder="Vorname (optional)"></span>
				<span id="nname"><input type="text" id="nachname" class="form-control" value="${param.nachname}" placeholder="Nachname (optional)"></span> 
				<span id="geb"><input type="text" id="geb" name="geb" class="form-control" value="${param.geb}"  onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Geburtsdatum (optional)(z. B. 31.01.2014)"></span>				
				<button type="submit" class="position_button btn btn-default btn-color btn-register">Registrieren</button>
			</fieldset>
		</form>
	</div>
</div>

<%
@SuppressWarnings("unchecked")
String alert = (String)request.getAttribute("alert");
if(alert != null){
	out.println("<div class=\"alert alert-danger alert-dismissable\" style=\"position:absolute; top:60px; right:300px; z-index:1000;\">");
	out.println("<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>");
	out.println("<strong>" + alert + "</strong>");
	out.println("</div>");
}
%>


</html>
