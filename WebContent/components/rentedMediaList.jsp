<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="de">
<script>
$(document).ready(function(){ 
	  $("#verliehen").click(function(){
		  $("#alleTable").hide();
		  $("#verliehenTable").show();
	  });
	  $("#alle").click(function(){
		  $("#alleTable").show();
		  $("#verliehenTable").hide();
	  });
});
</script>

    <jsp:useBean id="myLibraryView" class="de.dhbw.media2share.view.MyLibraryView"></jsp:useBean>
		<h2>Mediathek</h2>
		<br>
		[Endpoint] Diese Seite zeigt eine Übersicht der eigenen Medien oder der aktuell an andere verliehenen Medien. Diese Seite war ursprünglich nicht geplant und ist daher noch nicht vollständig. Wir fanden es nur schade, sie nicht mit einzureichen. Aktuell sieht man eine Liste der Medien die von Benutzer Ralf an Benutzerin an Laura Siebenstein verliehen wurden. Die Verleihzeit ist zufällig und verändert sich durch Aktualisieren (F5). 
		<br>
		<div id="searchPanel" class="well">
			<form role="search">
				<div class="row">
					<div class="col-md-1">
						Medien
					</div>
					<div class="col-md-5">
						<div data-toggle="buttons">
							<label class="btn btn-default"><input type="radio" name="options" id="alle"> Alle </label>
							<label class="btn btn-default active"><input type="radio" name="options" id="verliehen"> Verliehen </label>
						</div>
					</div>
				</div>
			</form>
		</div>
		
		<table class="table" id="verliehenTable">
		<thead>
			<tr>
				<td class="text-left"><h3><span class="label label-primary">Titel</span></h3></td>
				<td class="text-left"><h3><span class="label label-primary">An</span></h3></td>
				<td class="text-left"><h3><span class="label label-primary">Seit</span></h3></td>
			</tr>
		</thead>   
        <tbody>
        	<c:forEach var="listItem" items="${myLibraryView.listOfRentedMedias}">
        		<tr>
        			<td colspan="3"><h3><span class="label label-${listItem.color}">${listItem.description} Tage</span></h3></td>
        		</tr>
        		  <c:forEach var="myLibraryViewRentObject" items="${listItem.list}">
		          		<tr class="${myLibraryViewRentObject.color}">
							<td class="text-left"><h4>${myLibraryViewRentObject.title}</h4></td>
		              		<td class="text-left"><h4>${myLibraryViewRentObject.renter}</h4></td>
		              		<td class="text-left"><h4><span class="label label-${myLibraryViewRentObject.color}">${myLibraryViewRentObject.days}</span></h4></td>
		          		</tr>
		          </c:forEach> 
		     </c:forEach>
        </tbody>  
      </table>
</html>