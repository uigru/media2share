<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
        <div class="sidebarLeft">
          <div class="nav nav-sidebar">
          		<form action="Search" method="GET">
          				<input type="text" name="searchfield"  value="${param.searchfield}" placeholder="Suchen" list="suggestions"/>
          				<datalist id="suggestions">
							<option value="The dark knight"/>
							<option value="Django unchained"/>
							<option value="Two and a half Men Staffel 1"/>
							<option value="Dschungelbuch"/>
							<option value="Kindsköpfe 2"/>
							<option value="Titanic"/>
							<option value="KeinOhrHasen"/>
							<option value="Avatar"/>
							<option value="Black Swan"/>
							<option value="Vom Winde verweht"/>
							<option value="Bad"/>
							<option value="Spiceworld"/>
							<option value="Spektakulär"/>
							<option value="Ein ganzes halbes Jahr"/>
							<option value="Tödliche Saat - Ostfrieslandkrimi"/>
							<option value="BLACKOUT - Morgen ist es zu spät"/>
							<option value="No Line on the Horizon"/>
							<option value="The Truth About Love"/>
							<option value="Bravo Hits Vol.84"/>
							<option value="Die Analphabetin, die rechnen konnte"/>
							<option value="Tschick"/>
							<option value="Er ist wieder da"/>
							<option value="Lieder"/>
							<option value="Taking chances"/>
							<option value="Farbenspiel"/>
							<option value="Die Bibel: Altes und Neues Testament. Einheitsübersetzung"/>
							<option value="Der Knochenbrecher"/>
							<option value="Der Junge im gestreiften Pyjama"/>
							<option value="MGMT"/>
							<option value="To Beast Or Not to Beast"/>
							<option value="The Essential"/>
							<option value="G I R L"/>
							<option value="50 Shades of Grey"/>
							<option value="Pleasures of the Damned"/>
							<option value="300"/>
							<option value="Captain America"/>
					</datalist>
						<button type="submit" name="submit"><span class="glyphicon glyphicon-search"></span></button>
				</form>
          </div>
          <ul class="nav nav-sidebar">
            <li class="active"><a href="myLibrary.jsp">Bibliothek</a></li>
            <li><a href="EndpointAnfragen.jsp">Anfragen</a></li>
            	<ul class="nav-sidebar-sublist">
            		<li><a href="EndpointErhalten.jsp">Erhalten</a> <span class="badge">3</span></li>
            		<li><a href="EndpointGesendet.jsp">Gesendet</a> <span class="badge">1</span></li>
            		<li><a href="EndpointEvents.jsp">Events</a> <span class="badge">7</span></li>
          		</ul>
          </ul>	
          <ul class="nav nav-sidebar">
            <li><a href="EndpointEinstellungen.jsp">Einstellungen</a></li>
          </ul>
        </div>     
</html>