<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.*, de.dhbw.media2share.view.ListItem, de.dhbw.media2share.model.MediaObject"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script>
$(document).ready(function(){
	  $("#hide").click(function(){
		  $("#togglePanel").toggle();
		  $("#searchButton1").toggle();
		  $("#chevronUP").toggle();
		  $("#chevronDOWN").toggle();
	  });
});
</script>

<div id="searchPanel" class="well">
	<form role="search" action="Search" method="GET">
		<div class="row">
			<div class="col-md-3">
					<h4><input type="text" name="searchfield" value="${param.searchfield}" placeholder="Suchen" list="suggestions"/>
					<datalist id="suggestions">
							<option value="The dark knight"/>
							<option value="Django unchained"/>
							<option value="Two and a half Men Staffel 1"/>
							<option value="Dschungelbuch"/>
							<option value="Kindsköpfe 2"/>
							<option value="Titanic"/>
							<option value="KeinOhrHasen"/>
							<option value="Avatar"/>
							<option value="Black Swan"/>
							<option value="Vom Winde verweht"/>
							<option value="Bad"/>
							<option value="Spiceworld"/>
							<option value="Spektakulär"/>
							<option value="Ein ganzes halbes Jahr"/>
							<option value="Tödliche Saat - Ostfrieslandkrimi"/>
							<option value="BLACKOUT - Morgen ist es zu spät"/>
							<option value="No Line on the Horizon"/>
							<option value="The Truth About Love"/>
							<option value="Bravo Hits Vol.84"/>
							<option value="Die Analphabetin, die rechnen konnte"/>
							<option value="Tschick"/>
							<option value="Er ist wieder da"/>
							<option value="Lieder"/>
							<option value="Taking chances"/>
							<option value="Farbenspiel"/>
							<option value="Die Bibel: Altes und Neues Testament. Einheitsübersetzung"/>
							<option value="Der Knochenbrecher"/>
							<option value="Der Junge im gestreiften Pyjama"/>
							<option value="MGMT"/>
							<option value="To Beast Or Not to Beast"/>
							<option value="The Essential"/>
							<option value="G I R L"/>
							<option value="50 Shades of Grey"/>
							<option value="Pleasures of the Damned"/>
							<option value="300"/>
							<option value="Captain America"/>
					</datalist>
					</h4>
			</div>
			<div class="col-md-3">
				<button id="searchButton1" type="submit"  class="btn btn-success" style="position: absolute; top:7px;">Suchen</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" style="float: right;">
				<a href="#" id="hide">Erweiterte Suche  </a><span id="chevronUP" style="display: none" class="glyphicon glyphicon-chevron-up"></span><span id="chevronDOWN" class="glyphicon glyphicon-chevron-down"></span>
			</div>
		</div>
		<%String categories = (String)request.getParameter("categories");
		  String area = (String)request.getParameter("area");
		  					String area1 ="";
		  					String area2="";
		  					String aarea1="";
		  					String aarea2="";
							String active1 = "";
							String active2 = "";
							String active3 = "";
							String active4 = "";
							String checked1 = "";
							String checked2 = "";
							String checked3 = "";
							String checked4 = "";
							if(categories != null){ 
								if(categories.equals("option1")){
									checked1="checked=\"checked\"";
									active1="active";
								}else if(categories.equals("option2")){
									checked2="checked=\"checked\"";
									active2="active";
								}else if(categories.equals("option3")){
									checked3="checked=\"checked\"";
									active3="active";
								}else if(categories.equals("option4")){
									checked4="checked=\"checked\"";
									active4="active";
							    }
							}
							else{
								checked1="checked=\"checked\"";
								active1="active";
							}
							if(area != null){ 
								if(area.equals("option1")){
									aarea1="checked=\"checked\"";
									area1="active";
								}else if(area.equals("option2")){
									aarea2="checked=\"checked\"";
									area2="active";
								}
							}else{
								aarea1="checked=\"checked\"";
								area1="active";
							}
									
		%>
		<div id="togglePanel" style="display: none">
			<div class="row">
				<br>
			</div>
			<div class="row">
				<br>
			</div>
			<div class="row">
				<div class="col-md-2">Kategorie</div>
				<div class="col-md-5">
					<div data-toggle="buttons">
						<label class="btn btn-default <%out.print(active1);%>"><input type="radio" name="categories" value="option1" <%out.print(checked1);%>> Alle </label>
						<label class="btn btn-default <%out.print(active2);%>"><input type="radio" name="categories" value="option2" <%out.print(checked2);%>><span class="glyphicon glyphicon-book"></span> Bücher </label>
						<label class="btn btn-default <%out.print(active3);%>"><input type="radio" name="categories" value="option3" <%out.print(checked3);%>><span class="glyphicon glyphicon-film"></span> Filme </label>
						<label class="btn btn-default <%out.print(active4);%>"><input type="radio" name="categories" value="option4" <%out.print(checked4);%>><span class="glyphicon glyphicon-music"></span> Musik</label>
					</div>
				</div>
			</div>
			<div class="row">
				<br>
			</div>
			<div class="row">
				<div class="col-md-2">Personenkreis</div>
				<div class="col-md-5">
					<div data-toggle="buttons">
						<label class="btn btn-default <%out.print(area1);%>"><input type="radio" name="area" value="option1" <%out.print(aarea1);%>><span class="glyphicon glyphicon-user"></span> Alle </label>
						<label class="btn btn-default <%out.print(area2);%>"><input type="radio" name="area" value="option2" <%out.print(aarea2);%>><span class="glyphicon glyphicon-star"></span> Nur Freunde </label>
					</div>
				</div>
			</div>
			<div class="row">
				<br>
			</div>
			<div class="row">
				<div class="col-md-2">
					im Umkreis von
				</div>
				<div class="col-md-5">
					<input name="distance"  value="${param.distance}" type="text"> km
				</div>
			</div>
			<div class="row">
				<br>
				<div class="col-md-2 col-md-offset-10">
					<button type="submit" class="btn btn-success">Suchen</button>
				</div>
			</div>
		</div>
	</form>
</div>

		<%
			@SuppressWarnings("unchecked")
			ListItem item  = (ListItem) request.getAttribute("listItem");
			if ( item != null) {
				if(item.getSize() != 0){%>
		<h3><span class="label label-primary">Ergebnisse deiner Suche:    ( <%out.print(item.getSize());%> Treffer )</span></h3>
		<br>
		<br>
		<ul class="media-list">
			<%if(!(item.getListBook().size() == 0)) {%>
			<li class="media"><a class="pull-left" href="#"></a>
				<div class="media-body" style="background-color:#5ED613; border-radius:20px;">
					<h3 class="media-heading" style="color:white; margin-left: 15px;">Bücher</h3>
				</div>
			</li>
			<% for(MediaObject m : item.getListBook()){ %>
				<li class="media"><a class="pull-left" href="#"><img
						height="128px" width="128px" class="media-object"
						src="resources/img/medias/<%out.print(m.getPicture());%>.jpg" alt="<%out.print(m.getTitle());%>"></a>
					<div class="media-body">
						<h4 class="media-heading"><%out.print(m.getTitle());%> (<%out.print(m.getMediaTypeString());%>)   <button class="btn" style="background-color:#5ED613; color:white;" onclick="this.style.visibility='hidden'">Leihen</button></h4>
						<%out.print(m.getDescription());%>
					</div>
				</li>
			<%} %>	
			<%} %>
			<%if(!(item.getListFilm().size() == 0)) {%>
			<li class="media"><a class="pull-left" href="#">
			</a>
				<div class="media-body" style="background-color:#E6710B; border-radius:20px;">
					<h3 class="media-heading" style="color:white; margin-left: 15px;">Filme</h3>
				</div>
			</li>
			<% for(MediaObject m : item.getListFilm()){ %>
				<li class="media"><a class="pull-left" href="#"><img
						height="128px" width="128px" class="media-object"
						src="resources/img/medias/<%out.print(m.getPicture());%>.jpg" alt="<%out.print(m.getTitle());%>"></a>
					<div class="media-body">
						<h4 class="media-heading"><%out.print(m.getTitle());%> (<%out.print(m.getMediaTypeString());%>)    <button class="btn" style="background-color:#E6710B; color:white;" onclick="this.style.visibility='hidden'">Leihen</button></h4>
						<%out.print(m.getDescription());%>
					</div>
				</li>
			<% } %>
			<% } %>
			<%if(!(item.getListMusic().size() == 0)) {%>
			<li class="media"><a class="pull-left" href="#">
			</a>
				<div class="media-body" style="background-color:#0C3369; border-radius:20px;">
					<h3 class="media-heading" style="color:white; margin-left: 15px;">Musik</h3>
				</div>
			</li>
			<% for(MediaObject m : item.getListMusic()){ %>
				<li class="media"><a class="pull-left" href="#"><img
						height="128px" width="128px" class="media-object"
						src="resources/img/medias/<%out.print(m.getPicture());%>.jpg" alt="<%out.print(m.getTitle());%>"></a>
					<div class="media-body">
						<h4 class="media-heading"><%out.print(m.getTitle());%> (<%out.print(m.getMediaTypeString());%>)   <button class="btn" style="background-color:#0C3369; color:white;" onclick="this.style.visibility='hidden'">Leihen</button></h4>
						<%m.getDescription();%>
					</div>
				</li>
			<%} %>
			<% } %>
		</ul>
		<% }else{%>
			<h3><span class="label label-danger">Die Suche erzielte keine Treffer.</span></h3>
		<%}} %>
</html>