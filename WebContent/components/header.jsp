<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="de">
    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="Main.jsp">XAME</a>
        </div>
        
        <ul class="nav navbar-nav">
  			<li><a href="EndpointMitteilungen.jsp">Mitteilungen <span class="badge" style="color:white; background-color:red"> 3 </span></a></li>
		</ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="EndpointEinstellungen.jsp"><span class="glyphicon glyphicon-user"></span></a></li>
            <li><a href="login.jsp"><span class="glyphicon glyphicon-off"></span>  Abmelden</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
</html>