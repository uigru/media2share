<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    
    <script type="text/javascript">
      function createPolarChart() {
    		var data = [
    		        	{
    		        		value : 59,
    		        		color: "#0C3369"
    		        	},
    		        	{
    		        		value : 120,
    		        		color: "#E6710B"
    		        	},
    		        	{
    		        		value : 71,
    		        		color: "#5ED613"
    		        	}
    		        ];
    		var options2 = {
    				
    				//Boolean - Whether we show the scale above or below the chart segments
    				scaleOverlay : true,
    				
    				//Boolean - If we want to override with a hard coded scale
    				scaleOverride : true,
    				
    				//** Required if scaleOverride is true **
    				//Number - The number of steps in a hard coded scale
    				scaleSteps : 1,
    				//Number - The value jump in the hard coded scale
    				scaleStepWidth : 100,
    				//Number - The centre starting value
    				scaleStartValue : 0,
    				
    				//Boolean - Show line for each value in the scale
    				scaleShowLine : false,
    				
    				//String - The colour of the scale line
    				scaleLineColor : "rgba(0,0,0,.1)",
    				
    				//Number - The width of the line - in pixels
    				scaleLineWidth : 1,
    				
    				//Boolean - whether we should show text labels
    				scaleShowLabels : false,

    				
    				//String - Scale label font declaration for the scale label
    				scaleFontFamily : "'Arial'",
    				
    				//Number - Scale label font size in pixels	
    				scaleFontSize : 0.0000000005,
    				
    				//String - Scale label font weight style	
    				scaleFontStyle : "normal",
    				
    				//String - Scale label font colour	
    				scaleFontColor : "#FFF",
    				
    				//Boolean - Show a backdrop to the scale label
    				scaleShowLabelBackdrop : true,
    				
    				//String - The colour of the label backdrop	
    				scaleBackdropColor : "rgba(255,255,255,0.75)",
    				
    				//Number - The backdrop padding above & below the label in pixels
    				scaleBackdropPaddingY : 2,
    				
    				//Number - The backdrop padding to the side of the label in pixels	
    				scaleBackdropPaddingX : 2,

    				//Boolean - Stroke a line around each segment in the chart
    				segmentShowStroke : false,
    				
    				//String - The colour of the stroke on each segement.
    				segmentStrokeColor : "#fff",
    				
    				//Number - The width of the stroke value in pixels	
    				segmentStrokeWidth : 2,
    				
    				//Boolean - Whether to animate the chart or not
    				animation : true,
    				
    				//Number - Amount of animation steps
    				animationSteps : 100,
    				
    				//String - Animation easing effect.
    				animationEasing : "easeOutBounce",

    				//Boolean - Whether to animate the rotation of the chart
    				animateRotate : true,
    				
    				//Boolean - Whether to animate scaling the chart from the centre
    				animateScale : false,

    				//Function - This will fire when the animation of the chart is complete.
    				onAnimationComplete : null,

    				showTooltips : false
    		};
    		var options = {
    				
    				//Boolean - Whether we show the scale above or below the chart segments
    				scaleOverlay : true,
    				
    				//Boolean - If we want to override with a hard coded scale
    				scaleOverride : true,
    				
    				//** Required if scaleOverride is true **
    				//Number - The number of steps in a hard coded scale
    				scaleSteps : 1,
    				//Number - The value jump in the hard coded scale
    				scaleStepWidth : 100,
    				//Number - The centre starting value
    				scaleStartValue : 0,
    				
    				//Boolean - Show line for each value in the scale
    				scaleShowLine : false,
    				
    				//String - The colour of the scale line
    				scaleLineColor : "rgba(0,0,0,.1)",
    				
    				//Number - The width of the line - in pixels
    				scaleLineWidth : 1,
    				
    				//Boolean - whether we should show text labels
    				scaleShowLabels : false,

    				
    				//String - Scale label font declaration for the scale label
    				scaleFontFamily : "'Arial'",
    				
    				//Number - Scale label font size in pixels	
    				scaleFontSize : 0.0000000005,
    				
    				//String - Scale label font weight style	
    				scaleFontStyle : "normal",
    				
    				//String - Scale label font colour	
    				scaleFontColor : "#FFF",
    				
    				//Boolean - Show a backdrop to the scale label
    				scaleShowLabelBackdrop : true,
    				
    				//String - The colour of the label backdrop	
    				scaleBackdropColor : "rgba(255,255,255,0.75)",
    				
    				//Number - The backdrop padding above & below the label in pixels
    				scaleBackdropPaddingY : 2,
    				
    				//Number - The backdrop padding to the side of the label in pixels	
    				scaleBackdropPaddingX : 2,

    				//Boolean - Stroke a line around each segment in the chart
    				segmentShowStroke : false,
    				
    				//String - The colour of the stroke on each segement.
    				segmentStrokeColor : "#fff",
    				
    				//Number - The width of the stroke value in pixels	
    				segmentStrokeWidth : 2,
    				
    				//Boolean - Whether to animate the chart or not
    				animation : true,
    				
    				//Number - Amount of animation steps
    				animationSteps : 100,
    				
    				//String - Animation easing effect.
    				animationEasing : "easeOutBounce",

    				//Boolean - Whether to animate the rotation of the chart
    				animateRotate : true,
    				
    				//Boolean - Whether to animate scaling the chart from the centre
    				animateScale : false,

    				//Function - This will fire when the animation of the chart is complete.
    				onAnimationComplete : null,

    				showTooltips : true
    		};
    		var cht = document.getElementById('polarChart');
    		var ctx = cht.getContext('2d');
    		var polarChart = new Chart(ctx).PolarArea(data,options2);

    		var cht2 = document.getElementById('polarChart2');
    		var ctx2 = cht2.getContext('2d');
    		var polarChart2 = new Chart(ctx2).PolarArea(data,options);
    		}
		</script>
		<script>
		$(document).ready(function(){
			  $("#charles").mouseover(function(){
			    $("#textcharles").slideDown();
			  });
			});
		$(document).ready(function(){
			  $("#dreihundert").mouseover(function(){
			    $("#text300").slideDown();
			  });
			});
		$(document).ready(function(){
			  $("#elvis").mouseover(function(){
			    $("#textelvis").slideDown();
			  });
			});
		$(document).ready(function(){
			  $("#sof").mouseover(function(){
			    $("#text50sof").slideDown();
			  });
			});
		$(document).ready(function(){
			  $("#captain").mouseover(function(){
			    $("#textcaptain").slideDown();
			  });
			});
		$(document).ready(function(){
			  $("#pharell").mouseover(function(){
			    $("#textpharell").slideDown();
			  });
			});
		
		$(document).ready(function(){
			  $("#z0").mouseover(function(){
			    $("#text300").slideUp();
			    $("#textcharles").slideUp();
			    $("#textelvis").slideUp();
			    $("#text50sof").slideUp();
			    $("#textcaptain").slideUp();
			    $("#textpharell").slideUp();
			  });
			});
		$(document).ready(function(){
			  $("#z1").mouseover(function(){
			    $("#text300").slideUp();
			    $("#textcharles").slideUp();
			    $("#textelvis").slideUp();
			    $("#text50sof").slideUp();
			    $("#textcaptain").slideUp();
			    $("#textpharell").slideUp();
			  });
			});

		$(document).ready(function(){
			  $( "#Musik" ).slideUp( 450 ).delay( 700 ).fadeIn( 900 );
			  $( "#z2elvis" ).slideUp( 450 ).delay( 700 ).fadeIn( 900 );
			  $( "#z2pharell" ).slideUp( 450 ).delay( 700 ).fadeIn( 900 );

			  $( "#Filme" ).slideUp( 450 ).delay( 1300 ).fadeIn( 900 );
			  $( "#z2captain" ).slideUp( 450 ).delay( 1300 ).fadeIn( 900 );
			  $( "#z2300" ).slideUp( 450 ).delay( 1300 ).fadeIn( 900 );

			  $( "#Buecher" ).slideUp( 450 ).delay( 1800 ).fadeIn( 900 );
			  $( "#z250sof" ).slideUp( 450 ).delay( 1800 ).fadeIn( 900 );
			  $( "#z2charles" ).slideUp( 450 ).delay( 1800 ).fadeIn( 900 );
			});

</script>

<div id="z0">
<canvas id="polarChart" width="2000px" height="2000px"></canvas>
</div>
<div id="z1">
<canvas id="polarChart2" width="600px" height="600px"></canvas>
</div>
<h2><span id="Buecher" class="label label-default">Bücher</span></h2>
<h2><span id="Musik" class="label label-default">Musik</span></h2>
<h2><span id="Filme" class="label label-default">Filme</span></h2>
<div id="z2charles">
<div id="charles"><img id="charles" src="resources/img/medias/33.jpg" alt="charles bukowski" width="48" height="65"></div>
<div id="textcharles">Charles Bukowski<br /><br />Pleasures of the Damned&nbsp;<br /><br />2004</div>
</div>

<div id="z250sof">
<div id="sof"><img id="sof" src="resources/img/medias/32.jpg" alt="50 Shades of Grey" width="48" height="65"></div>
<div id="text50sof">E.L. James<br /><br />50 Shades of Grey&nbsp;<br /><br />2012</div>
</div>

<div id="z2300">
<div id="dreihundert"><img id="dreihundert" src="resources/img/medias/34.jpg" alt="300" width="48" height="65"></div>
<div id="text300">300<br /><br />Rise of an Empire&nbsp;<br /><br />2014</div>
</div>

<div id="z2captain">
<div id="captain"><img id="captain" src="resources/img/medias/35.jpg" alt="Captain America" width="48" height="65"></div>
<div id="textcaptain">Captain America<br /><br />The Winter Soldier&nbsp;<br /><br />2014</div>
</div>

<div id="z2elvis">
<div id="elvis"><img id="elvis" src="resources/img/medias/30.jpg" alt="Elivs" width="48" height="65"></div>
<div id="textelvis">Elvis Presley<br /><br />The Essential&nbsp;<br /><br />2000</div>
</div>

<div id="z2pharell">
<div id="pharell"><img id="pharell" src="resources/img/medias/31.jpg" alt="Pharell" width="48" height="65"></div>
<div id="textpharell">Pharell Williams<br /><br />G I R L&nbsp;<br /><br />2014</div>
</div>
<script src="resources/js/extendChart.js"></script>
</html>