<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landing Page after Login">
    <meta name="author" content="Clemens Diebold">
    
    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom Media2Sare CSS -->
	<link href="resources/css/media2share.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
 
    <title>Xame</title>
  </head> 
  <body>
  	<jsp:include page="components/header.jsp"></jsp:include>
	<div class="container-fluid">
      	<div class="row">
      		<div class="col-sm-3 col-md-2">
				<jsp:include page="components/sidebarLeft.jsp"></jsp:include>
      		</div>
      		<div class="col-sm-3 col-md-8">
      			<div class="main">
      				Main Content of the Page
      			</div>
      		</div>
      		<div class="col-sm-3 col-md-2">
				<jsp:include page="components/sidebarRight.jsp"></jsp:include>
      		</div>
		
  	 	</div>
  	 </div>
  	 <jsp:include page="components/footer.jsp"></jsp:include>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
  </body>
</html>