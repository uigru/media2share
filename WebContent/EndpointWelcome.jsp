<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landing Page after Login">
    <meta name="author" content="Clemens Diebold">
    
    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom Media2Sare CSS -->
	<link href="resources/css/media2share.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<title>Xame</title>
</head>
<body>
  	<jsp:include page="components/header.jsp"></jsp:include>
	<div class="container-fluid">
      	<div class="row">
      		<div class="col-sm-3 col-md-2">
				<jsp:include page="components/sidebarLeft.jsp"></jsp:include>
      		</div>
      		<div class="col-sm-3 col-md-8">
      			<div class="main">
      			
      				<h3><span class="label label-success">Willkommen bei Xame!</span></h3>
      				[Endpoint] Der Benutzer würde hier eine kleine Einführung bekommen mit Bidern und kurzen Erläuterungen zu den wichtigsten Funktionen.
      				Achtung! Die gerade eingegebenen Daten wurden nicht geischert. Der von Ihnen angelegte Benutzer ist auf der Plattform nicht verfügbar.
      				
      				<br>
      				<br>
      				<br>
      				<button type="submit" onclick="location.href='Main.jsp'" class="btn btn-success">Fortfahren</button>
      			</div>
      		</div>
      		<div class="col-sm-3 col-md-2">
				<jsp:include page="components/sidebarRight.jsp"></jsp:include>
      		</div>
		
  	 	</div>
  	 </div>
  	 <jsp:include page="components/footer.jsp"></jsp:include>
</body>
</html>