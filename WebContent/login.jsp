<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Login Page">
	<meta name="author" content="Clemens Diebold, Christopher Link">
	
	<title>Xame</title>
	<link href="resources/css/js-image-slider.css" rel="stylesheet" type="text/css" />
	<script src="resources/js/js-image-slider.js" type="text/javascript"></script>
	<link href="resources/css/generic.css" rel="stylesheet" type="text/css" />
	
	<!-- Bootstrap core CSS -->
	<link href="resources/css/bootstrap.min.css" rel="stylesheet" />
	
	<!-- media2share general 	CSS NOT IN USE IN LOGIN.JSP
	    <link href="resources/css/media2share.css" rel="stylesheet" /> -->
	
	<!-- login.media2share CSS -->
	<link href="resources/css/login.media2share.css" rel="stylesheet" media="screen"/>
	
	
	<!-- Just for debugging purposes. Don't actually copy this line! -->
	<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->

	<script>
    	$(document).ready(function(){

    		// Validation
    		$("#registerHere").validate({
    		rules:{
    			user_name:"required",
    			user_email:{required:true,email: true},
    			pwd:{required:true,minlength: 6},
    			cpwd:{required:true,equalTo: "#pwd"},
    			gender:"required"
    		},

    		messages:{
    		user_name:"Enter your first and last name",
    		user_email:{
    		required:"Enter your email address",
    		email:"Enter valid email address"},
    		pwd:{
    		required:"Enter your password",
    		minlength:"Password must be minimum 6 characters"},
    		cpwd:{
    		required:"Enter confirm password",
    		equalTo:"Password and Confirm Password must match"},
    		gender:"Select Gender"
    		},

    		errorClass: "help-inline",
    		errorElement: "span",
    		highlight:function(element, errorClass, validClass)
    		{
    		$(element).parents('.control-group').addClass('error');
    		},
    		unhighlight: function(element, errorClass, validClass)
    		{
    		$(element).parents('.control-group').removeClass('error');
    		$(element).parents('.control-group').addClass('success');
    		}
    		});
    		});

    </script>
</head>

<body>

	<div class="loginSiteMobile">
		<div class="mobileSidePosition">
			<div class="container cPosition">
				<div class="jumbotron">
					<div class="mobileHeader">
						<h4>Hole Dir Deine mobile App aufs Handy</h4>
					</div>
					<img src="resources/img/app_store.jpg">
					<img src="resources/img/play_store.jpg">
				</div>
			</div>
		</div>
	</div>

	<div class="loginSite">

		<div class="navbar navbar-inverse navbar-fixed-top navbar-opacity" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span> 
						<span class="icon-bar"></span>
					</button>

					<!-- <a class="navbar-brand" href="#">Xame</a>  -->
				</div>

				<div class="navbar-collapse collapse">
					<form class="navbar-form navbar-right" method="GET" action="Login">
						<div class="form-group">
							<input name="username" type="text" value="${param.username}" placeholder="E-Mail Adresse" class="form-control">
						</div>
						<div class="form-group">
							<input name="password" type="password" placeholder="Passwort" class="form-control">
						</div>
						<button type="submit" class="btn btn-success">Login</button>
					</form>
				</div>
				<!--/.navbar-collapse -->
			</div>
		</div>

		<div class="container" id="midPage">

			<!-- Xame Logo<img class="xame_logo"src="resources/img/Logo_rund.png"/>  -->

			<div class="row">
				<div class="col-lg-4">
					<div class="jumbotron">
						<div class="evaluation_header">
							<h4>Das sagen User über Xame</h4>
						</div>
						<p id="eva_1">"Xame ist ein klasse Tool um sich mit Freunden zu treffen und einen gemütlichen Filmeabend zu veranstalten.",Laura</p>
						<p id="eva_2">"Endlich habe ich eine Webanwendung gefunden um mir Filme in meiner Nähe ausleihen zu können. Jetzt bin ich abends auch mal vor dem Fernseher anstatt vor dem PC. I love it!", Ralf</p>
					</div>
				</div>

				<div id="sliderFrame">
					<div>
						<button type="button" id="registerButton" onclick="location.href='#openModal'" class="btn btn-color btn-boxsize register-button btn-register">Registrieren</button>
					</div>

					<div id="slider">
						<a href="resources/js/js-image-slider.js"> <img src="resources/img/Fluch der Karibik skaliert.jpg" alt="Freue Dich auf spannende Filme." /></a> 
							<img src="resources/img/Schnuffel.jpg" alt="" />
							<img src="resources/img/Music-Is-Medicine-Facebook-Cover.jpg" alt="Immer die aktuellste Musik." />
							<img src="resources/img/Buch3.jpg" alt="Greife auf eine Vielfalt von Topsellern zu." />
							<img src="resources/img/Buch2.jpg" />
					</div>
				</div>
			</div>
		</div>

		<jsp:include page="components/footer.jsp"></jsp:include>
		<jsp:include page="components/loginPanel.jsp"></jsp:include>
	</div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/docs.min.js"></script>
</body>
</html>